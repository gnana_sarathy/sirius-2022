package com.dao;

import java.util.List;
import com.model.TrainingRoom;

public interface TrainingRoomDAO {
     
    List<TrainingRoom> findAllRooms();
    
    TrainingRoom getRoom(int roomId);
    void updateStatus(int roomId);
}
