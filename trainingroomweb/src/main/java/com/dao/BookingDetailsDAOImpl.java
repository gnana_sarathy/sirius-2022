package com.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.model.BookingDetails;

@Repository
public class BookingDetailsDAOImpl extends AbstractDAO<Integer, BookingDetails> implements BookingDetailsDAO{

	@Override
	public BookingDetails getDetail(int roomId) {
		Criteria criteria =  createEntityCriteria();
		BookingDetails bd = (BookingDetails)criteria.add(Restrictions.eq("roomId", roomId)).uniqueResult();
		return bd;
	}

	@Override
	public void saveDetail(BookingDetails bd) {
		persist(bd);
	}
	
	

}
