package com.dao;

import com.model.BookingDetails;

public interface BookingDetailsDAO {
	BookingDetails getDetail(int roomId);
	void saveDetail(BookingDetails bd);
}
