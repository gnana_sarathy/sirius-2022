package com.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.model.TrainingRoom;

@Repository("traingroom")
public class TrainingRoomDAOImpl extends AbstractDAO<Integer, TrainingRoom> implements TrainingRoomDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<TrainingRoom> findAllRooms() {
		Criteria criteria = createEntityCriteria();
        return (List<TrainingRoom>) criteria.list();
	}

	@Override
	public TrainingRoom getRoom(int roomId) {
		// TODO Auto-generated method stub
		Criteria criteria =  createEntityCriteria();
        TrainingRoom tr=(TrainingRoom)criteria.add(Restrictions.eq("roomId", roomId)).uniqueResult();
		return tr;
	}

	@Override
	public void updateStatus(int roomId) {
		Criteria criteria =  createEntityCriteria();
        TrainingRoom tr=(TrainingRoom)criteria.add(Restrictions.eq("roomId", roomId)).uniqueResult();
		tr.setStatus(1);
		update(tr);
	}
	
	
	
}
