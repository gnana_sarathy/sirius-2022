package com.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BookingDetails {
	@Id
	private int roomId;
	private String customerName;
	private int hours;
	private int amountPaid;
	private String paymentId;
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public int getHours() {
		return hours;
	}
	public void setHours(int hours) {
		this.hours = hours;
	}
	public int getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(int amountPaid) {
		this.amountPaid = amountPaid;
	}
	
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}	
	
	@Override
	public String toString() {
		return "BookingDetails [roomId=" + roomId + ", customerName=" + customerName + ", hours=" + hours
				+ ", amountPaid=" + amountPaid + ", paymentId=" + paymentId + "]";
	}
}
