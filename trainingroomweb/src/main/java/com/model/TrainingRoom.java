package com.model;

import java.sql.Blob;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TrainingRoom {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int roomId;
	private String roomName;
	private Blob roomImage;
	private int status;
	private String image;
	private int pricePerHour;
	private int capacity;
	private String location;
	
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public Blob getRoomImage() {
		return roomImage;
	}
	public void setRoomImage(Blob roomImage) {
		this.roomImage = roomImage;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getPricePerHour() {
		return pricePerHour;
	}
	public void setPricePerHour(int pricePerHour) {
		this.pricePerHour = pricePerHour;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Override
	public String toString() {
		return "TrainingRoom [roomId=" + roomId + ", roomName=" + roomName + ", roomImage=" + roomImage + ", status="
				+ status + ", image=" + image + ", pricePerHour=" + pricePerHour + ", capacity=" + capacity
				+ ", location=" + location + "]";
	}
}
