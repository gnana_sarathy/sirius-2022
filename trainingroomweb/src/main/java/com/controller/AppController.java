package com.controller;

import java.sql.Blob;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.model.BookingDetails;
import com.model.RazorPay;
import com.model.TrainingRoom;
import com.razorpay.Order;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.service.BookingDetailsService;
import com.service.EncodeImageService;
import com.service.RazorPayService;
import com.service.ResponseService;
import com.service.TrainingRoomService;


@Controller
@RequestMapping("/")
@ComponentScan("com") 
public class AppController {
	
	private RazorpayClient client;
	private static Gson gson = new Gson();

	private static final String SECRET_ID = "rzp_test_QvVmup8pcm8DkO";
	private static final String SECRET_KEY = "1V6oXC7FzAWc3YRieOORKcmK";
	
	public AppController() throws RazorpayException {
		this.client =  new RazorpayClient(SECRET_ID, SECRET_KEY); 
	}
	
	@Autowired
    EncodeImageService encodeImageService;
	
	@Autowired
    TrainingRoomService trainingRoomService;
    
    @Autowired
    BookingDetailsService bookingDetailsService;
    
    @Autowired
	RazorPayService rayzorpay;
    
    @Autowired
    ResponseService response;
     
    //    Method to list all Training Rooms
    @RequestMapping(value = {"/", "/dashboard" }, method = RequestMethod.GET)
    public String listTrainingRoom(ModelMap model) {
    	System.out.println("...................Training Room Called................");
        List<TrainingRoom> rooms = trainingRoomService.findAllRooms();
        for(TrainingRoom room : rooms)
        {
        	Blob blob = room.getRoomImage();
        	String encodedImage = encodeImageService.convert(blob);
        	room.setImage(encodedImage);
        }
        model.addAttribute("rooms", rooms);
        return "dashboard";
	    }
    
    //Method to view status of Training Room
    @RequestMapping(value = {"/status-{roomId}-{status}"}, method = RequestMethod.GET)
    public String viewRoom(@PathVariable int roomId, @PathVariable int status, ModelMap model) {
      if(status == 1)
      {
    	  BookingDetails bookingDetail = bookingDetailsService.getDetail(roomId);
    	  model.addAttribute("customerDetail", bookingDetail);
    	  return "alreadybooked";
      }
      else
      {
    	  TrainingRoom room = trainingRoomService.getRoom(roomId);
    	  BookingDetails detail = new BookingDetails();
    	  model.addAttribute("room", room);
    	  model.addAttribute("detail", detail);
    	  return "getDetail";
      }
    }
    
    //Method to get detail from user
    @RequestMapping(value = {"/status-{roomId}-{status}" }, method = RequestMethod.POST)
    public String saveDetail(@Valid BookingDetails detail, @PathVariable int roomId,ModelMap model) {
    	TrainingRoom room = trainingRoomService.getRoom(roomId);
    	int amount = detail.getHours() * room.getPricePerHour();
    	detail.setAmountPaid(amount);
    	model.addAttribute("detail", detail);
    	return "payment";
    }
    
    //Method to create order and payment
    @RequestMapping(value = {"/payment-{amount}-{name}" }, method = RequestMethod.POST)
    public ResponseEntity<String> createOrder(@PathVariable String amount, @PathVariable String name) {
		
		try {
			Order order = rayzorpay.createRazorPayOrder(client, amount);
			String OrderId = (String)order.get("id");
			RazorPay razorPay = rayzorpay.getRazorPay(OrderId, amount, name, SECRET_ID);
			return new ResponseEntity<String>(gson.toJson(response.getResponse(razorPay, 200)), HttpStatus.OK);
		} catch (RazorpayException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<String>(gson.toJson(response.getResponse(new RazorPay(), 500)), HttpStatus.EXPECTATION_FAILED);
	}
    
    //Method to update detail of user and training room status
    @RequestMapping(value = {"/update" }, method = RequestMethod.POST)
    public void testpost(@RequestBody BookingDetails detail)
    {
    	bookingDetailsService.saveDetail(detail);
    	trainingRoomService.updateStatus(detail.getRoomId());
    }
    
}

