package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.TrainingRoomDAO;
import com.model.TrainingRoom;

@Service("trainingRoomService")
@Transactional
public class TrainingRoomServiceImpl implements TrainingRoomService{
	@Autowired
    private TrainingRoomDAO dao;
	@Override
	public List<TrainingRoom> findAllRooms() {
		return dao.findAllRooms();
	}
	
	@Override
	public TrainingRoom getRoom(int roomId) {
		return dao.getRoom(roomId);
	}

	@Override
	public void updateStatus(int roomId) {
		dao.updateStatus(roomId);
		
	}
}
