package com.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.BookingDetailsDAO;
import com.model.BookingDetails;

@Service("bookingDetailsService")
@Transactional
public class BookingDetailsServiceImpl implements BookingDetailsService{
	@Autowired
    private BookingDetailsDAO dao;
	
	@Override
	public BookingDetails getDetail(int roomId) {
		// TODO Auto-generated method stub
		return dao.getDetail(roomId);
	}

	@Override
	public void saveDetail(BookingDetails bd) {
		// TODO Auto-generated method stub
		dao.saveDetail(bd);
		
	}
}
