package com.service;

import com.model.RazorPay;
import com.model.Response;

public interface ResponseService {
	Response getResponse(RazorPay razorPay, int statusCode);
}
