package com.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.model.RazorPay;
import com.model.Response;


@Service
@Transactional
public class ResponseServiceImpl implements ResponseService{

	@Override
	public Response getResponse(RazorPay razorPay, int statusCode) {
		// TODO Auto-generated method stub
		Response response = new Response();
		response.setStatusCode(statusCode);
		response.setRazorPay(razorPay);
		return response;
	}
	

}
