package com.service;

import com.model.BookingDetails;

public interface BookingDetailsService {
	BookingDetails getDetail(int roomId);
	void saveDetail(BookingDetails bd);
}
