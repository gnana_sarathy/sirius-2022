package com.service;

import java.sql.Blob;

public interface EncodeImageService {
	String convert(Blob blob);
	
}
