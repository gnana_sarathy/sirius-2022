package com.service;

import java.util.List;

import com.model.TrainingRoom;


public interface TrainingRoomService {
	List<TrainingRoom> findAllRooms(); 
	TrainingRoom getRoom(int roomId);
	void updateStatus(int roomId);
}
