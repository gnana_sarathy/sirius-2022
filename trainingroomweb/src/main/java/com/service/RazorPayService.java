package com.service;

import com.model.RazorPay;
import com.model.Response;
import com.razorpay.Order;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

public interface RazorPayService {
	Response getResponse(RazorPay razorPay, int statusCode);
	RazorPay getRazorPay(String orderId, String amount, String name, String SECRET_ID);
	Order createRazorPayOrder(RazorpayClient clien, String amount) throws RazorpayException;
}
