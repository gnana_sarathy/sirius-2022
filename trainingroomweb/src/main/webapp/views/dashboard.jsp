<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <title>Training Room Dashboard</title>
	    <style><%@include file="../resources/styles/dashboard.css"%></style>
	 	<link rel="stylesheet" href=""/>
	    <link rel="preconnect" href="https://fonts.googleapis.com">
	    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	    <link href="https://fonts.googleapis.com/css2?family=Luckiest+Guy&family=Overpass:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
	</head>
 
	<body>
	
	    <header>
	        <div class="header-container">
	            <h1>Training Room Dashboard</h1>
	        </div>
	    </header>
	    
	    <h2></h2>
	            <div class="card-wrapper">
	
			        <c:forEach items="${rooms}" var="room">
			        	<div class="card">
				        	<div class="card-contents">
					        	<img src="data:image/jpg;base64,${room.image}" width="240" height="300"/>
					        	<h3>${room.roomName}</h3>
					        	<h4>Capacity: ${room.capacity}</h4>
					        	
	                            	<a href="<c:url value='/status-${room.roomId}-${room.status}'/>"><div class="button blue">View Status</div></a>
	                        	
	                        	<h5>&#8377; ${room.pricePerHour}<span>per hour</span></h5>
	                        	<h6>Location: ${room.location}</h6>
				        	</div>
			        	</div>
			        </c:forEach>
			    </div>
	
	</body>
</html>