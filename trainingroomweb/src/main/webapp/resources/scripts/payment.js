/**
 * 
 */
 
 function doPostObj(path, requestObject, isAsync) {
		
		$.ajax({
            url: path,
            type: 'POST',
            data: requestObject,
            contentType: "application/json; charset=UTF-8",
            async: isAsync
            
       })
	}
	function doPost(path, isAsync) {
		var resp;
			$.ajax({
	            url: path,
	            type: 'POST',
	            async: isAsync,
	            success: function (data) {	
	            	resp = JSON.parse(data)
	            }
	       })
	       return resp;
		}
	
	function doGet(path, isAsync)
	{
		$.ajax({
			url:path,
			type:"GET",
			async:isAsync
			})
	}
		
		
	var resp = null;
	
	var options = {
		    "key": "",
		    "amount": "", 
		    "name": "",
		    "description": "",
		    "image": "",
		    "order_id":"",
		    "handler": function (response){
		    	var val = document.getElementById('amount').getAttribute('data-id');
		    	var roomId = document.getElementById('roomId').getAttribute('data-id');
		    	var name = document.getElementById('name').getAttribute('data-id');
		    	var hours = document.getElementById('hours').getAttribute('data-id');
		    	var payId = response.razorpay_payment_id;
		    	var obj = {roomId:roomId, amountPaid:val, customerName:name, hours:hours, paymentId:payId};

					document.getElementById('payment-failed-modal').style.display="none";
                    document.getElementById('overlay').style.display="none";
		    	
		    	doPostObj("/update", JSON.stringify(obj), false);
		    	$('#success').show();
		    	$('#payment').hide();
		    	
		    },
		    "prefill": {
		        "name": "",
		        "email": ""
		    },
		    "notes": {
		        "address": ""
		    },
		    "theme": {
		        "color": ""
		    }
		};
		
		
		document.getElementById('rzp-button1').onclick = function(e){
			var val = document.getElementById('amount').getAttribute('data-id');
			var name = document.getElementById('name').getAttribute('data-id');
			var url = "/payment-" + val + "-" + name;
			
			resp = doPost(url, false);
			if(resp.statusCode == 200) {
				options.key = resp.razorPay.secretKey;
				options.order_id = resp.razorPay.razorpayOrderId;
				options.amount = resp.razorPay.applicationFee; //paise
				options.name = resp.razorPay.merchantName;
				options.description = resp.razorPay.purchaseDescription;
				options.image = resp.razorPay.imageURL;
				options.prefill.name = resp.razorPay.customerName;
				options.prefill.email = resp.razorPay.customerEmail;
				options.notes.address = resp.razorPay.notes;
				options.theme.color = resp.razorPay.theme;
				var rzp1 = new Razorpay(options);
				rzp1.open();
				
				rzp1.on('payment.failed', function (response){
                    document.getElementById('payment-failed-modal').style.display="block";
                    document.getElementById('overlay').style.display="block";
		            
		    	});
				e.preventDefault();
			}
		}
		
		document.getElementById("footer-cta").onclick = function () {
			closeModal();
		};
		
		//To close payment failed modal
        function closeModal() {

            document.getElementById('payment-failed-modal').style.display="none";
            document.getElementById('overlay').style.display="none";
        }