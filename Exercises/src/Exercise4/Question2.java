package Exercise4;

class Sample_2{
	final void printer(){
		System.out.println("Hello i am from parent");
	}
}

class Child extends Sample_2{
//	final void printer()
//	{
//		System.out.println("I am not allowed ^_^");
//	}
}

public class Question2 {
	public static void main(String[] args) {
		Sample_2 s = new Sample_2();
		Child child =  new Child();
		s.printer();
		child.printer();
		
	}

}
