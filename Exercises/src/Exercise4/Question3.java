//package Exercise4;
//
//final class Parent{
//	void aboutMe()
//	{
//		System.out.println("I can't be inherited, so don't try..");
//	}
//}
//
//class SubClass extends Parent{
//	void aboutMe()
//	{
//		System.out.println("I am inherited only without Final..");
//	}
//}
//
//public class Question3 {
//	public static void main(String[] args) {
//		Parent parent = new Parent();
//		parent.aboutMe();
//		SubClass child = new SubClass();
//		child.aboutMe();
//	}
//}
