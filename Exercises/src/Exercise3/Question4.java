package Exercise3;

class Sample4_A{
	Sample4_A()
	{
		System.out.println("I am constructor of Sample_2A");
	}
	
}

class Sample4_B extends Sample4_A{
	
	Sample4_B()
	{
		super();
		System.out.println("I am constructor of Sample_2B");
	}
	
	
}

public class Question4 {
	public static void main(String[] args) {
		Sample4_B b = new Sample4_B();
		
	}
}
