package Exercise3;

class Sample1_A{
	int i, j;
	public void setI(int i) {
		this.i = i;
	}
	public void setJ(int j) {
		this.j = j;
	}
	void print()
	{
		System.out.println("From Class A");
		System.out.println("value of i " + i);
		System.out.println("value of j " + j);
	}
}

class Sample1_B extends Sample1_A{
	int k;
	public void setK(int k) {
		this.k = k;
	}

	void print()
	{
		System.out.println("From Class B");
		System.out.println("value of i " + i);
		System.out.println("value of i " + j);
		System.out.println("value of k " + k);
	}
	
	void sum()
	{
		System.out.println("sum of i, j and k is " + (i+j+k));
	}
}

public class Question1 {
	public static void main(String[] args) {
		Sample1_A obja = new Sample1_A();
		Sample1_B objb = new Sample1_B();

		obja.setI(5);
		obja.setJ(10);

		objb.setI(5);
		objb.setJ(10);
		objb.setK(15);
		
		obja.print();
		objb.print();
		
		objb.sum();
		
		
	}
}
