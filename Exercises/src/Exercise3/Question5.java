package Exercise3;

class Sample5_A{
	Sample5_A()
	{
		System.out.println("I am constructor of Sample_3A");
	}
}

class Sample5_B extends Sample5_A{
	Sample5_B()
	{
		System.out.println("I am constructor of Sample_3B");
	}
}

class Sample5_C extends Sample5_B{
	Sample5_C()
	{
		System.out.println("I am constructor of Sample_3C");
	}
}

public class Question5 {
	public static void main(String[] args) {
		Sample5_C c = new Sample5_C();
		
	}
}
