package Exercise3;

interface Sample7_A{
	void play();
}

interface Sample7_B{
	void walk();
}

class Sample7_C implements Sample7_A, Sample7_B{
	@Override
	public void walk() {
		System.out.println("I am walking..");
	}

	@Override
	public void play() {
		System.out.println("I am playing..");
	}
	void dance()
	{
		System.out.println("I am dancing...");
	}
}

public class Question7 {
	public static void main(String[] args) {
		Sample7_C c = new Sample7_C();
		c.walk();
		c.play();
		c.dance();
		
	}

}