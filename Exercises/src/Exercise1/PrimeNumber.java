package Exercise1;
import java.util.Scanner;

public class PrimeNumber {
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		sc.close();
		boolean flag = true;
		
		for(int i = 2; i < number; i++){
			if(number % i == 0)
			{
				System.out.println("Not Prime Number");
				flag = false;
				break;
			}
		}
		
		if(flag)
		{
			System.out.println("Prime number");
		}
	}

}
