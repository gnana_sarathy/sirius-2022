package Exercise1;

public class Exercise27 {
	public static void main(String[] args) {
		float initial_amount = 14000f;
		int first_profit = 40;
		// initial amount increased by 40 %
		initial_amount += calculateProfit(initial_amount, first_profit);
		
		//on second year initial amount drop by 1500 dollar
		initial_amount -= 1500;
		
		int second_profit = 12;
		initial_amount += calculateProfit(initial_amount, second_profit);
		
		System.out.println(initial_amount);
		
	
	}
	public static float calculateProfit(float initial_amount, float profit)
	{
		profit = profit * .01f;
		return (initial_amount * profit);
	}

}
