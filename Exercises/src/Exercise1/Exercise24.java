package Exercise1;

class Quadratic{
	private int a;
	private int b;
	private int c;
	Quadratic()
	{
		this.a =1;
		this.b = 1;
		this.c =1;
	}
	
	Quadratic(int a, int b, int c)
	{
		this.a = a;
		this.b = b;
		this.c = c;
	}
	void setvaluea(int a)
	{
		this.a = a;
	}
	void setvalueb(int b)
	{
		this.b = b;
	}
	void setvaluec(int c)
	{
		this.c = c;
	}
	
	int getvaluea()
	{
		return this.a;
	}
	int getvalueb()
	{
		return this.b;
	}
	int getvaluec()
	{
		return this.c;
	}
	
	
	int calculate(int x)
	{
		int ans = (this.a * x * x) + (this.b * x) +this.c;
		return ans;
	}
	
}
public class Exercise24 {
	public static void main(String[] args) {
		Quadratic q = new Quadratic(2, 3, 4);
		System.out.println(q.calculate(3));
	}	
}
