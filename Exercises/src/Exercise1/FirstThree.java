package Exercise1;

public class FirstThree {
	static int i;
	static long l;
	static float f;
	static double d; 
	static boolean b;
	static String s;
	static byte by;
	public static void main(String[] args)
	{
		System.out.println("Hello World");
		
		System.out.println("****************************\n");
		System.out.println("default value of interger "+ i);
		System.out.println("default value of long "+ l);
		System.out.println("default value of float "+ f);
		System.out.println("default value of double "+ d);
		System.out.println("default value of boolean "+ b);
		System.out.println("default value of string "+ s);
		System.out.println("default value of byte "+ by);
		
		System.out.println("****************************\n");
		
//		Assigning value to primitives
		int value1 = 5;
		float value2 = 5.25f;
		double value3 = 5.89;
		byte value4 = 125;
		char value5 = 'a';
		String value6 = "apple";
		System.out.println("Value of integer " + value1);
		System.out.println("Value of float " + value2);
		System.out.println("Value of double " + value3);
		System.out.println("Value of byte " + value4);
		System.out.println("Value of char " + value5);
		System.out.println("Value of String " + value6);
		
		System.out.println("****************************\n");
		
//		Implicit type casting short to large
		byte variable1 = 10;
		int variable2 = variable1;
		float variable3 = variable1;
		double variable4 = variable1;
		short variable5 = variable1;
		long variable6 = variable1;
		System.out.println("implicit type cast from byte to int "+ variable2);
		System.out.println("implicit type cast from byte to float "+ variable3);
		System.out.println("implicit type cast from byte to double "+ variable4);
		System.out.println("implicit type cast from byte to short "+ variable5);
		System.out.println("implicit type cast from byte to long "+ variable6);
		
		
		System.out.println("****************************\n");
		
//		Explicit type casting large to short
		
		double variable_1 = 10.2;
		float variable_2 = (float) variable_1;
		long variable_3 = (long) variable_1;
		int variable_4 = (int) variable_1;
		short variable_5 =(short) variable_1;
		byte variable_6 = (byte) variable_1;
		
		int ascii = 97;
		char ch = (char) ascii;
		System.out.println("Explicit type cast from double to float "+ variable_2);
		System.out.println("Explicit type cast from double to long "+ variable_3);
		System.out.println("Explicit type cast from double to int "+ variable_4);
		System.out.println("Explicit type cast from double to short "+ variable_5);
		System.out.println("Explicit type cast from double to byte "+ variable_6);
		
		System.out.println("Explicit type cast from int to character "+ ch);
		
		
		
	}
}
