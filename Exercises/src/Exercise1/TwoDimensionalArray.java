package Exercise1;


public class TwoDimensionalArray {
	public static void main(String[] args) {
		int row = 4, column = 4;
		int[][] array= {
				{1, 2, 3, 4},
				{5, 6, 7, 8},
				{9, 10, 11, 12},
				{13, 14, 15, 16}};
		
		for(int i = 0;i < row; i++)
		{
			for(int j = 0; j< column; j++)
			{
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("***************");
		
//		int start = 1;
//		int rows = 4;
//		for(int i = 0;i< rows;i++)
//		{
//			for(int j=0; j<=i; j++)
//			{
//				System.out.print(start++ + " ");
//			}
//			System.out.println();
//		}
		int[][] triangle = {
				{1}, {2, 3},
				{4, 5, 6},
				{7, 8, 9, 10}};
		for(int i=0; i<triangle.length;i++)
		{
			for(int j=0;j<triangle[i].length;j++)
			{
				System.out.print(triangle[i][j]+" ");
			}
			System.out.println();
		}
	}

}
