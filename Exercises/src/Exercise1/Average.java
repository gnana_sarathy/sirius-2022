package Exercise1;
import java.util.Scanner;

public class Average {
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter total number of values:");
		int n = sc.nextInt();
		int []arr = new int[n];
		System.out.println("Enter values for array:");
		for(int i=0; i<n;i++)
		{
			arr[i] = sc.nextInt();
		}
		sc.close();
		float answer = average(arr, n);
		System.out.println("average value is " + answer);
	}
	public static float average(int[] array, int n)
	{
		int sum = 0;
		for(int i=0; i <n; i++)
		{
			sum += array[i];
		}
		return sum/n;
	}

}
