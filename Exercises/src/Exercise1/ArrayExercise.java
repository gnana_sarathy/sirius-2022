package Exercise1;
import java.util.Arrays;

public class ArrayExercise {
	public static void main(String args[])
	{
		int[] array = {3, 5, 1};
		int extend = 2;
		array = Arrays.copyOf(array, array.length + extend);
		array[3] =2;
		array[4] = 4;
		String order = "descending";
		
		if(order == "ascending")
		{
			array = sortAscending(array, array.length);
			display(array);
		}
		else {
			array = sortDescending(array, array.length);
			display(array);
			
		}
		
		
		int element = 4;
		int index = findElement(array, element);
		if(index == -1)
		{
			System.out.println("Element not found");
		}
		else
		{
			System.out.println("\nLocation of element "+ element + " is " + index);
		}
		
		
		reversing(array);
	}
	
	public static int[] sortAscending(int[] array, int n)
	{
		int temp;
		System.out.println("Ascending order...");
		for(int i = 0; i < n; i++)
		{
			for(int j=0; j<n-i-1; j++)
			{
				if(array[j+1] < array[j])
				{
					temp = array[j+1];
					array[j+1] = array[j];
					array[j] = temp;
				}
			}
		}
		return array;
	}
	
	public static int[] sortDescending(int[] array, int n)
	{
		int temp;
		System.out.println("Descending order...");
		for(int i = 0; i < n; i++)
		{
			for(int j=0; j<n-i-1; j++)
			{
				if(array[j+1] > array[j])
				{
					temp = array[j+1];
					array[j+1] = array[j];
					array[j] = temp;
				}
			}
		}
		return array;
	}
	
	public static void display(int[] array)
	{
		for(int i=0; i<array.length;i++)
		{
			System.out.println(array[i]);
		}
		
	}
	
	public static int findElement(int[] array, int element)
	{
		int index = -1;
		for(int i=0; i<array.length;i++)
		{
			if(element == array[i])
			{
				index = i;
			}
		}
		return index;
		
	}
	
	public static void reversing(int[] array)
	{
		System.out.println("\nReversing array....");
		for(int i = array.length-1; i>=0; i--)
		{
			System.out.println(array[i]);
		}
	}
	
	
}
