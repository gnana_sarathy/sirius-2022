package Exercise1;

class SampleClass{
	protected int data = 100;
}

public class Exercise23 {
	public static void main(String[] args) {
		int value = modify(200);
		System.out.println(value);
	}
	public static int modify(int data)
	{
		SampleClass obj = new SampleClass();
		obj.data = data;
		return obj.data;
	}
}
