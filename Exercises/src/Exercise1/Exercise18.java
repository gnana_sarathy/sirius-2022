package Exercise1;
import java.util.Scanner;

public class Exercise18 {
	public static void main(String[] args) {
		String [] words = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		sc.close();
		String str = String.valueOf(number);
		for(int i = 0; i<str.length(); i++)
		{
			int index = Character.getNumericValue(str.charAt(i));
			System.out.print(words[index]+ " ");
		}
		
	}

}
