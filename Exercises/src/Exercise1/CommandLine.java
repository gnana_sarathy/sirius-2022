package Exercise1;

public class CommandLine {
	public static void main(String[] args) {
		System.out.println("Hello "+ args[0]);
		int number = Integer.parseInt(args[1]);
		if(number%2 == 0)
		{
			System.out.println("Given number is even");
		}
		else
		{
			System.out.println("Given number is odd");
		}
	}

}
