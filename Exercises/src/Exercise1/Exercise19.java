package Exercise1;

public class Exercise19 {
	public static void main(String[] args) {
		int[][] array = {
				{1, 2, 3, 4, 5},
				{5, 3, 1, 2, 1},
				{2, 0, 4, 1, 6},
				{5, 2, 6, 1, 7}};
		
		for(int i = 0; i<array.length; i++)
		{
			for(int j = array[0].length-1; j>=0; j--)
			{
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}

}
