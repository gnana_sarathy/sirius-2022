package Exercise1;

public class Exercise20 {
	public static void main(String[] args) {
		int[][] array = {
				{1, 2, 3, 4, 5},
				{5, 3, 1, 2, 1},
				{2, 0, 4, 1, 6},
				{5, 2, 6, 1, 7}};
		
		for(int i = 0; i < array.length; i++)
		{
			int maximum = -1;
			for(int j = 0; j<array[0].length; j++)
			{
				if(array[i][j] > maximum)
				{
					maximum = array[i][j];
				}
			}
			System.out.println("Largest Element in row " + (i+1) +" is " + maximum);
		}
	}

}
