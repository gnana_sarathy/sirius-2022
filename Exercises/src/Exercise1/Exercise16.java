package Exercise1;

public class Exercise16 {

	public static void main(String[] args) {
		int[] array = {1, 16, 32, 24, 1, 12, 45, 12, 8};
		int element = 13, count =0;
		
		for(int i = 0; i < array.length; i++)
		{
			if(array[i] == element)
			{
				count++;
				if(count == 1)
				{
					System.out.println("Element present at index of : " + i);
				}
			}
		}
		if(count == 0)
		{
			System.out.println("Element is not present");
		}
		else {
			System.out.println("Element " + element + " present " + count + " times");
		}
	}
}
