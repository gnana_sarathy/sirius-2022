package Exercise1;

public class Result {
	public static void main(String[] args) {
		
		int total_students = 4, total_subjects = 5;
		
		int [][] marksheet = {
				{80, 90, 75, 60, 83},
				{76, 98, 76, 53, 90},
				{59, 90, 68, 93, 88},
				{78, 89, 58, 90, 90}};
		
		int maximum_mark = 0, index = -1;
		
		for(int i = 0; i<total_students; i++)
		{
			int total = 0;
			for(int j = 0; j < total_subjects; j++)
			{
				total += marksheet[i][j];
			}
			if(total> maximum_mark)
			{
				maximum_mark = total;
				index = i;
			}
		}
		index++;
		System.out.println("Maximum mark attain by student roll no: "+ index);
		
	}

}
