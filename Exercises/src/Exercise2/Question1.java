package Exercise2;

class SampleA{
	int add(int a, int b)
	{
		System.out.println("SampleA First add method was called");
		return a + b;
	}
	
	int add(int a, int b, int c)
	{
		System.out.println("SampleA Second add method was called");
		return a + b + c;
	}
}

class SampleB{
	int add(int a, int b)
	{
		System.out.println("SampleB First add method was called");
		return a + b;
	}
	
	double add(double a, double b)
	{
		System.out.println("SampleB second add method was called");
		return a+b;
	}
}

//class SampleC{
//	int add(int a, int b)
//	{
//		System.out.println("SampleC First add method was called");
//		return a + b;
//	}
//	
//	double add(int a, int b)
//	{
//		System.out.println("SampleC Second add method was called");
//		return (a + b)/2;
//	}
//}

class SampleD{
	void sum(int a, long b)
	{
		System.out.println("SampleD first method was called");
		System.out.println(a+b);
	}
	void sum(long a, int b)
	{
		System.out.println("SampleD second method was called");
		System.out.println(a+b);
	}
}



class ClassA{
	int add(int a, int b)
	{
		System.out.println("From Class A");
		return a+b;
	}
}
class ClassB extends ClassA{
	int add(int a, int b)
	{
		System.out.println("From Class B");
		return a+ b;
	}
}

public class Question1 {
	public static void main(String[] args) {
		int i1 = 5, i2 = 10, i3 = 15;
		SampleA a = new SampleA();
		System.out.println(a.add(i1, i2));
		System.out.println(a.add(i1, i2, i3));
		
		int v1 = 5, v2 =10;
		double d1 = 12.3, d2 = 34.4;
		SampleB b = new SampleB();
		System.out.println(b.add(v1, v2));
		System.out.println(b.add(d1, d2));
		
		int x = 5, y = 10;
		long l1 = 10, l2 =20;
		
		SampleD d = new SampleD();
		d.sum(x, l1);
		d.sum(l2, y);
		
		
		//over riding concepts
		ClassA class_a = new ClassA();
		System.out.println(class_a.add(80, 15));
		
		ClassB class_b = new ClassB();
		System.out.println(class_b.add(90, 14));
		
		
	}
}
